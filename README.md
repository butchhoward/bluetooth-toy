# Bluetooth Toy

Create a Bluetooth toy that notifies you when a friend is near.

## Ideation for Future Features
https://jamboard.google.com/d/1WbkFACLERrqKTy2FUVoM-xlHEUs7tsV9ev6EbJwNZR8/edit?usp=sharing


## Hardware Notes

Need to define custom board (see include/custom_board.h) and map to actual device for LEDs and buttons.  Note that Arduino corresponding pins for Bluefruit are defined.

Bluefruit uses an nrf52840 processor.  Nearest equivalent dev board is 10056.

[Schematics to find pin assignments](https://learn.adafruit.com/adafruit-circuit-playground-bluefruit/downloads)

[Nordic Semiconductor SDKs](https://www.nordicsemi.com/Software-and-tools/Software/nRF5-SDK/Download)